'''
Created on May 26, 2014

@author: lauril
'''

import json
import numpy
from pprint import pprint

class TwoDSimpleGrid(object):
    '''
    classdocs
    '''


    def __init__(self, config):
        '''
        Grid class, used for holding the grid size and obstacles
        obsticles are read from a json file
        class respond to isBusy for a x,y coordinate
        '''
        self.section = 'Grid'
        self.obj=json.loads(config)
        self.grid = numpy.asmatrix(self.obj['grid'], int)
        self.obsticles = []
        self.dimm = self.grid.shape
        self.coolision = 0

        
    def isBlocked(self, x, y):
        shape=self.grid.shape
        if x>shape[0]-1 or y>shape[1]-1 or x <0 or y <0:
            return True
        

        return self.grid[x,y] >0
    
    def addAgent(self,x,y):
        if self.grid[x,y] ==2:
            self.coolision+=1
            print "COLLISION #%d with agent" %self.coolision
            return False
        elif self.grid[x,y] ==1:
            self.coolision+=1
            print "COLLISION #%d with wall" %self.coolision
            return False
        else:
            self.grid[x,y]=2
            return True
    
    def removeAgent(self,x,y):
        self.grid[x,y]=0
        
    def getMatrix(self):
        return self.grid
    
    def getObsticals(self):
        if len(self.obsticles) >0:
            return self.obsticles
        else:
            obst =[]
            shape = self.grid.shape
            for x in xrange(shape[1]):
                for y in xrange(shape[0]):
                    if self.grid[x,y] == 1:
                        obst.append((x,y))
            self.obsticles = map(list,zip(*obst))
            pprint(self.obsticles)
                