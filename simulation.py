'''
Created on May 26, 2014

@author: lauril
'''
import matplotlib
matplotlib.use('TKAgg')

from grids.Grid import TwoDSimpleGrid
from agents.Agents import RandomWalkAgent
from pprint import pprint
import numpy as np
import matplotlib.pylab as plt
import matplotlib.animation as animation



#read config file
file_directory = 'simex.json'
json_data=open(file_directory).read()
grid = TwoDSimpleGrid(json_data)
agents =[]
total_agents=3
current= 0
color_data = np.random.random((200, total_agents))
x, y, c = np.random.random((3, 1))


fig = plt.figure()
scat = plt.scatter(x, y, c=c, s=100)

for x in range(0,total_agents):
    agents.append(RandomWalkAgent(grid,x,x,0))
for x in range(0,total_agents):
    agents[x].tryMove()




def animate(i,grid, scat, current):
    print "Animate %s" %i
    agents[current].tryMove()
    if current <total_agents:
        current+=1
    else:
        current=0
    arr = np.random.random((1, 1))
    scat.set_array(arr[0])
    
    return scat,



ani = animation.FuncAnimation(fig, animate, frames=xrange(100),
                                  fargs=(color_data, scat, current))
plt.show()

