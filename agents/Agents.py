'''
Created on May 26, 2014

@author: lauril
'''
from random import randint
from helpers import switch
import sys

class RandomWalkAgent(object):
    '''
    classdocs
    '''


    def __init__(self,  grid,Aid, x, y):
        '''
        Constructor
        '''
        self.grid = grid
        self.aid=Aid
        self.cpf = 0 #facing dirrection 0-3
        self.cpx = x
        self.cpy = y
        self.vision = 1
        if not self.grid.addAgent(self.cpx, self.cpy):
            if not self.grid.addAgent(self.cpx+1, self.cpy):
                if not self.grid.addAgent(self.cpx, self.cpy+1):
                    sys.exit(-1)                    
        
    def walk(self,x,y):
        x_t = self.cpx+x
        y_t = self.cpx+y
        if x_t < self.grid.dimm[0] and y_t < self.grid.dimm[1]:
            if (self.grid.addAgent( x_t, y_t )):
                self.grid.removeAgent(self.cpx, self.cpy)
                self.cpx=x_t
                self.cpy=y_t

        #print "advancing %s position (%s,%s)" %(self.aid,self.cpx, self.cpy)
        
    def seeForwards(self):
        direction = randint(0,3)
        x_to=y_to=0
        walk=(0,0)
        for case in switch(direction):
            if case(0):
                x_to=self.cpx-self.vision
                y_to=self.cpy
                walk=(-1,0)
                break
            if case(1):
                x_to=self.cpx
                y_to=self.cpy+self.vision
                walk=(0,1)
                break
            if case(2):
                x_to=self.cpx+self.vision
                y_to=self.cpy
                walk=(1,0)
                break
            if case(3):
                x_to=self.cpx
                y_to=self.cpy-self.vision
                walk=(0,-1)
                break
        if not self.grid.isBlocked(x_to, y_to):
            self.walk(walk[0], walk[1])

            
        
    def tryMove(self):
        self.seeForwards()
        
    def currentPossition(self):
        return (self.cpx, self.cpy)