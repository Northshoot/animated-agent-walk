'''
Created on May 27, 2014

@author: lauril
'''
'''
Created on May 27, 2014

@author: lauril
'''
#!/usr/bin/env python

import matplotlib
matplotlib.use('TKAgg')
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from agents.Agents import RandomWalkAgent
from grids.Grid import TwoDSimpleGrid

n = 3 ## nr of agents
x,y = 10, 10 ## matrix of x by y dimension
dataX, dataY, binaryRaster = [],[],[]
file_directory = 'simex.json'
json_data=open(file_directory).read()

class AnimatedScatter(object):
    """An animated scatter plot using matplotlib.animations.FuncAnimation."""
    def __init__(self):
        global n, json_data
        self.numpoints = n
        self.grid = TwoDSimpleGrid(json_data)
        self.fig, self.ax = plt.subplots()
        self.ax.set_title("Agent Based Model (ABM)",fontsize=14)
        self.ax.grid(True,linestyle='-',color='0.75')
        self.agents = []
        self.current = 0
        self.obsticles = self.grid.getObsticals()
        randX, randY = self.createRandomData()

        for x in xrange(n):
            self.agents.append(RandomWalkAgent(self.grid,x, randX[x], randY[x]))
        self.ani = animation.FuncAnimation(self.fig, self.update,interval=100, 
                                           init_func=self.setup_plot, blit=True,
                                           repeat=True)

    def agentPossitions(self):
        pos_tuple = []
        for x in xrange(n):
            pos_tuple.append(self.agents[x].currentPossition())
        return map(list, zip(*pos_tuple))
        
    def setup_plot(self):
        """Initial drawing of the scatter plot."""
        global x,y
        dataX , dataY = self.agentPossitions()
        self.obsticles =  self.grid.getObsticals()
        self.ax.scatter(self.obsticles[1], self.obsticles[0], c="b", marker='s',s=600, animated=False)
        self.scat = self.ax.scatter(dataY, dataX, c="tomato", s=600, animated=True)
        self.ax.axis([0, x, 0, y])
        #start, end = self.ax.get_xlim()
        self.ax.xaxis.set_ticks(np.arange(-0.5, 10.5, 1))
        self.ax.yaxis.set_ticks(np.arange(-0.5, 10.5,  1))

        return self.scat,

    """
    Simulation is run when the update is called from animation function
    one move of one agent is performed during one tick
    """
    def update(self, i):
        if self.current <self.numpoints:
            self.agents[self.current].tryMove()
            self.current+=1
        else:
            self.current=0
            self.agents[self.current].tryMove()
            self.current+=1

        dataX,  dataY =  self.agentPossitions()
        self.scat = self.ax.scatter(dataX, dataY, c="tomato", s=500, animated=True)
        return self.scat,

    def show(self):
        plt.show()

    def createRandomData(self):
        """Positions n agents randomly on a raster of x by y cells.
        Each cell can only hold a single agent."""

        global x,y,n
        binaryData = np.zeros((x,y), dtype=np.int)
        newAgents = 0
        dataX,dataY = [],[]
        while newAgents < n:
            row = np.random.randint(0,x,1)[0]
            col = np.random.randint(0,y,1)[0]
            if binaryData[row][col] != 1:
                binaryData[row][col] = 1
                newAgents+=1

        for row in range(x):
            for col in range(y):
                if binaryData[row][col] == 1:
                    dataX.append(row)
                    dataY.append(col)
        return dataX, dataY

def main():
    global n, x, y, dataX, dataY, binaryRaster
    a = AnimatedScatter()
    a.show()
    return

if __name__ ==  "__main__":
    main()
